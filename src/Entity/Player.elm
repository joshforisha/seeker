module Entity.Player exposing (generate)

import Component exposing (Component(..))
import Dice exposing (d6, roll)
import Entity exposing (Entity(..), addGen, attach)
import Random exposing (Generator)


generate : Generator Entity
generate =
    Entity.empty
        |> addGen Strength (roll 2 d6)
        |> Random.andThen (addGen Dexterity (roll 2 d6))
        |> Random.andThen (addGen Endurance (roll 2 d6))
        |> Random.andThen (addGen Intellect (roll 2 d6))
        |> Random.andThen (addGen Education (roll 2 d6))
        |> Random.andThen (addGen SocialStanding (roll 2 d6))
