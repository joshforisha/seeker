module View exposing (view)

import Array
import Html exposing (Html, a, div, h1, h2, h3, main_, p, text)
import Html.Attributes exposing (class)
import Model exposing (Model, Msg)
import View.InventoryPanel exposing (inventoryPanel)
import World exposing (World)
import World.Biome as Biome exposing (Biome)


view : Model -> Html Msg
view model =
    main_ []
        [ inventoryPanel model
        , case model.world of
            Just world ->
                div
                    [ class "content" ]
                    (world
                        |> Array.map
                            (Array.map (.biome >> Biome.toChar)
                                >> Array.toList
                                >> String.join ""
                            )
                        |> Array.toList
                        |> List.map (\r -> p [] [ text r ])
                    )

            Nothing ->
                text ""
        ]
