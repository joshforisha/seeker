module Component exposing (Component(..), is)


type Component
    = Dexterity Int
    | Education Int
    | Endurance Int
    | Intellect Int
    | Name String
    | SocialStanding Int
    | Strength Int


is : String -> Component -> Bool
is strType cmp =
    case ( strType, cmp ) of
        ( "dexterity", Dexterity _ ) ->
            True

        ( "education", Education _ ) ->
            True

        ( "endurance", Endurance _ ) ->
            True

        ( "intellect", Intellect _ ) ->
            True

        ( "name", Name _ ) ->
            True

        ( "social standing", SocialStanding _ ) ->
            True

        ( "strength", Strength _ ) ->
            True

        _ ->
            False
