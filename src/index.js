import './main.css'
import registerServiceWorker from './registerServiceWorker'
import { Elm } from './Main.elm'

Elm.Main.init({
  node: document.getElementById('Root')
})

registerServiceWorker()
