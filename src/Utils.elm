module Utils exposing (toHex)


toHex : Int -> String
toHex int =
    case int of
        10 ->
            "A"

        11 ->
            "B"

        12 ->
            "C"

        13 ->
            "D"

        14 ->
            "E"

        15 ->
            "F"

        _ ->
            String.fromInt int
