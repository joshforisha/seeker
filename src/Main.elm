module Main exposing (main)

import Browser
import Model exposing (Model, Msg, init, update)
import View exposing (view)


main : Program () Model Msg
main =
    Browser.element
        { init = \_ -> init
        , subscriptions = always Sub.none
        , update = update
        , view = view
        }
