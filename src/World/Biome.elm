module World.Biome exposing (Biome(..), toChar, toString)


type Biome
    = Desert
    | Forest
    | Jungle
    | Mountain
    | Ocean
    | Plains
    | Savanna
    | Taiga
    | Tundra


toChar : Biome -> String
toChar biome =
    case biome of
        Desert ->
            "🌵"

        Forest ->
            "🌳"

        Jungle ->
            "🌧️"

        Mountain ->
            "⛰️"

        Ocean ->
            "🌊"

        Plains ->
            "🌱"

        Savanna ->
            "🌴"

        Taiga ->
            "🌲"

        Tundra ->
            "❄️"


toString : Biome -> String
toString biome =
    case biome of
        Desert ->
            "Desert"

        Forest ->
            "Forest"

        Jungle ->
            "Jungle"

        Mountain ->
            "Mountain"

        Ocean ->
            "Ocean"

        Plains ->
            "Plains"

        Savanna ->
            "Savanna"

        Taiga ->
            "Taiga"

        Tundra ->
            "Tundra"
