module World.Region exposing (Region, create)

import World.Biome exposing (Biome(..))


type alias Region =
    { biome : Biome
    , elevation : Float
    , precipitation : Float
    , temperature : Float
    }


create : Float -> Float -> Float -> Region
create elev prec temp =
    let
        biome =
            if elev < 0 then
                Ocean
            else if elev >= 0.75 then
                Mountain
            else if prec >= -0.5 then
                if temp < -0.5 then
                    if prec < 0.25 then
                        Tundra
                    else
                        Taiga
                else if temp < 0 then
                    if prec < 0 then
                        Taiga
                    else if prec < 0.5 then
                        Plains
                    else
                        Forest
                else if temp < 0.5 then
                    if prec < 0.25 then
                        Plains
                    else
                        Forest
                else if prec < 0 then
                    Savanna
                else if prec < 0.5 then
                    Plains
                else
                    Jungle
            else
                Desert
    in
        { biome = biome
        , elevation = elev
        , precipitation = prec
        , temperature = temp
        }
