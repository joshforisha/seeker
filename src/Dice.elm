module Dice exposing (d6, roll, rollWith)

import Random exposing (Generator)


d6 : Generator Int
d6 =
    Random.int 1 6


roll : Int -> Generator Int -> Generator Int
roll num gen =
    if num == 1 then
        gen
    else
        Random.andThen
            (\r ->
                Random.map
                    (\s -> r + s)
                    (roll (num - 1) gen)
            )
            gen


rollWith : Int -> Generator Int -> List Int -> Generator Int
rollWith num gen dms =
    Random.map
        (\r -> r + List.sum dms)
        (roll num gen)
