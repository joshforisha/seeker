module Model exposing (Model, Msg(..), init, update)

import Entity exposing (Entity)
import Entity.Player as Player
import Random
import World exposing (World)


type Msg
    = Initialize
    | Setup ( Entity, World )


type alias Model =
    { entities : List Entity
    , player : Maybe Entity
    , world : Maybe World
    }


initialize : Cmd Msg
initialize =
    Random.generate Setup
        (Random.pair Player.generate (World.generate 34 17))


init : ( Model, Cmd Msg )
init =
    ( { entities = []
      , player = Nothing
      , world = Nothing
      }
    , initialize
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Initialize ->
            ( model, initialize )

        Setup ( player, world ) ->
            ( { model
                | player = Just player
                , world = Just world
              }
            , Cmd.none
            )
