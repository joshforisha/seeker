module View.InventoryPanel exposing (inventoryPanel)

import Component as Cmp exposing (Component(..))
import Entity
import Html exposing (Html, div, h1, text)
import Html.Attributes exposing (class)
import Model exposing (Model, Msg(..))
import Utils exposing (toHex)


inventoryPanel : Model -> Html Msg
inventoryPanel { player } =
    div
        [ class "inventory-panel" ]
        (case player of
            Just p ->
                [ h1 [] <|
                    List.filterMap identity
                        [ Maybe.map
                            (\cmp ->
                                case cmp of
                                    Strength i ->
                                        text (toHex i)

                                    _ ->
                                        text "?"
                            )
                            (Entity.get "strength" p)
                        , Maybe.map
                            (\cmp ->
                                case cmp of
                                    Dexterity i ->
                                        text (toHex i)

                                    _ ->
                                        text "?"
                            )
                            (Entity.get "dexterity" p)
                        , Maybe.map
                            (\cmp ->
                                case cmp of
                                    Endurance i ->
                                        text (toHex i)

                                    _ ->
                                        text "?"
                            )
                            (Entity.get "endurance" p)
                        , Maybe.map
                            (\cmp ->
                                case cmp of
                                    Intellect i ->
                                        text (toHex i)

                                    _ ->
                                        text "?"
                            )
                            (Entity.get "intellect" p)
                        , Maybe.map
                            (\cmp ->
                                case cmp of
                                    Education i ->
                                        text (toHex i)

                                    _ ->
                                        text "?"
                            )
                            (Entity.get "education" p)
                        , Maybe.map
                            (\cmp ->
                                case cmp of
                                    SocialStanding i ->
                                        text (toHex i)

                                    _ ->
                                        text "?"
                            )
                            (Entity.get "social standing" p)
                        ]
                ]

            Nothing ->
                []
        )
