module World exposing (World, generate)

import Array exposing (Array)
import Noise.Simplex exposing (noise2D)
import Random exposing (Generator, constant)
import World.Biome exposing (Biome(..))
import World.Region as Region exposing (Region)


type alias World =
    Array (Array Region)


generate : Int -> Int -> Generator World
generate width height =
    let
        heightFloat =
            toFloat height
    in
        Random.map2
            (\xseed yseed ->
                Array.initialize height
                    (\yo ->
                        let
                            yf =
                                toFloat yo

                            latitude =
                                -2 * abs ((yf / heightFloat) - 0.5)
                        in
                            Array.initialize width
                                (\xo ->
                                    let
                                        x =
                                            (xseed + toFloat xo)

                                        y =
                                            (yseed + yf)

                                        elevation =
                                            noise2D (0.2 * x) (0.2 * y)

                                        precipitation =
                                            noise2D (0.1 * x) (0.1 * y)

                                        temperature =
                                            noise2D (0.1 * x) (0.1 * y)
                                    in
                                        Region.create
                                            elevation
                                            precipitation
                                            ((0.25 * temperature) + (0.75 * latitude))
                                )
                    )
            )
            (Random.float 0 999)
            (Random.float 0 999)
