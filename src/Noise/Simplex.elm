module Noise.Simplex exposing (noise2D, noise3D, noise4D)

import Array exposing (Array)


aget : Int -> Array (Array a) -> Array a
aget =
    arrayGet Array.empty


arrayGet : a -> Int -> Array a -> a
arrayGet d i =
    Array.get i >> Maybe.withDefault d


dot : List Float -> List Float -> Float
dot xs ys =
    List.sum (List.map2 (*) xs ys)


e : Float
e =
    2.718281828459045


iget : Int -> Array Int -> Int
iget =
    arrayGet 0


init : List a -> List a
init list =
    List.take (List.length list - 1) list


lget : Int -> Array (Array a) -> List a
lget i arr =
    Array.toList (arrayGet Array.empty i arr)


noise2D : Float -> Float -> Float
noise2D x y =
    let
        f2 =
            0.5 * ((sqrt 3) - 1)

        s =
            (x + y) * f2

        i =
            floor (x + s)

        j =
            floor (y + s)

        g2 =
            ((3 - (sqrt 3)) / 6)

        t =
            (toFloat (i + j)) * g2

        x0 =
            x - (toFloat i - t)

        y0 =
            y - (toFloat j - t)

        i1 =
            if (x0 > y0) then
                1
            else
                0

        j1 =
            if (x0 > y0) then
                0
            else
                1

        x1 =
            x0 - toFloat i1 + g2

        y1 =
            y0 - toFloat j1 + g2

        x2 =
            x0 - 1 + 2 * g2

        y2 =
            y0 - 1 + 2 * g2

        ii =
            modBy 256 i

        jj =
            modBy 256 j

        gi0 =
            modBy 12 (iget (ii + (iget jj perm)) perm)

        gi1 =
            modBy 12 (iget (ii + i1 + (iget (jj + j1) perm)) perm)

        gi2 =
            modBy 12 (iget (ii + 1 + (iget (jj + 1) perm)) perm)

        t0 =
            0.5 - x0 * x0 - y0 * y0

        t1 =
            0.5 - x1 * x1 - y1 * y1

        t2 =
            0.5 - x2 * x2 - y2 * y2

        n0 =
            if (t0 < 0) then
                0
            else
                ((t0 ^ 4) * dot (init (lget gi0 gradients3D)) [ x0, y0 ])

        n1 =
            if (t1 < 0) then
                0
            else
                ((t1 ^ 4) * dot (init (lget gi1 gradients3D)) [ x1, y1 ])

        n2 =
            if (t2 < 0) then
                0
            else
                ((t2 ^ 4) * dot (init (lget gi2 gradients3D)) [ x2, y2 ])
    in
        70.0 * (n0 + n1 + n2)


noise3D : Float -> Float -> Float -> Float
noise3D x y z =
    let
        f3 =
            1 / 3

        s =
            (x + y + z) * f3

        i =
            floor (x + s)

        j =
            floor (y + s)

        k =
            floor (z + s)

        g3 =
            1 / 6

        t =
            toFloat (i + j + k) * g3

        x0 =
            (x - (toFloat i - t))

        y0 =
            (y - (toFloat j - t))

        z0 =
            (z - (toFloat k - t))

        ( i1, j1, k1 ) =
            if (x0 >= y0) then
                if (y0 >= z0) then
                    ( 1, 0, 0 )
                else
                    (if (x0 >= z0) then
                        ( 1, 0, 0 )
                     else
                        ( 0, 0, 1 )
                    )
            else
                (if (y0 < z0) then
                    ( 0, 0, 1 )
                 else
                    (if (x0 < z0) then
                        ( 0, 1, 0 )
                     else
                        ( 0, 1, 0 )
                    )
                )

        ( i2, j2, k2 ) =
            if (x0 >= y0) then
                if (y0 >= z0) then
                    ( 1, 1, 0 )
                else
                    ( 1, 0, 1 )
            else
                (if (y0 < z0) then
                    ( 0, 1, 1 )
                 else
                    (if (x0 < z0) then
                        ( 0, 1, 1 )
                     else
                        ( 1, 1, 0 )
                    )
                )

        x1 =
            x0 - toFloat i1 + g3

        y1 =
            y0 - toFloat j1 + g3

        z1 =
            z0 - toFloat k1 + g3

        x2 =
            x0 - toFloat i2 + 2 * g3

        y2 =
            y0 - toFloat j2 + 2 * g3

        z2 =
            z0 - toFloat k2 + 2 * g3

        x3 =
            x0 - 1 + 3 * g3

        y3 =
            y0 - 1 + 3 * g3

        z3 =
            z0 - 1 + 3 * g3

        ii =
            modBy 256 i

        jj =
            modBy 256 j

        kk =
            modBy 256 k

        gi0 =
            modBy 12 (iget (ii + (iget (jj + (iget kk perm)) perm)) perm)

        gi1 =
            modBy 12 (iget (ii + i1 + (iget (jj + j1 + (iget (kk + k1) perm)) perm)) perm)

        gi2 =
            modBy 12 (iget (ii + i2 + (iget (jj + j2 + (iget (kk + k2) perm)) perm)) perm)

        gi3 =
            modBy 12 (iget (ii + 1 + (iget (jj + 1 + (iget (kk + 1) perm)) perm)) perm)

        t0 =
            0.5 - x0 * x0 - y0 * y0 - z0 * z0

        t1 =
            0.5 - x1 * x1 - y1 * y1 - z1 * z1

        t2 =
            0.5 - x2 * x2 - y2 * y2 - z2 * z2

        t3 =
            0.5 - x3 * x3 - y3 * y3 - z3 * z3

        n0 =
            if (t0 < 0) then
                0
            else
                (t0 ^ 4) * (dot (lget gi0 gradients3D) [ x0, y0, z0 ])

        n1 =
            if (t1 < 0) then
                0
            else
                (t1 ^ 4) * (dot (lget gi1 gradients3D) [ x1, y1, z1 ])

        n2 =
            if (t2 < 0) then
                0
            else
                (t2 ^ 4) * (dot (lget gi2 gradients3D) [ x2, y2, z2 ])

        n3 =
            if (t3 < 0) then
                0
            else
                (t3 ^ 4) * (dot (lget gi3 gradients3D) [ x3, y3, z3 ])
    in
        32 * (n0 + n1 + n2 + n3)


noise4D : Float -> Float -> Float -> Float -> Float
noise4D x y z w =
    let
        f4 =
            ((sqrt 5) - 1) / 4

        g4 =
            (5 - sqrt 5) / 20

        s =
            (x + y + z + w) * f4

        i =
            floor (x + s)

        j =
            floor (y + s)

        k =
            floor (z + s)

        l =
            floor (w + s)

        t =
            toFloat (i + j + k + l) * g4

        x0 =
            x - (toFloat i - t)

        y0 =
            y - (toFloat j - t)

        z0 =
            z - (toFloat k - t)

        w0 =
            w - (toFloat l - t)

        c1 =
            if (x0 > y0) then
                32
            else
                1

        c2 =
            if (x0 > z0) then
                16
            else
                1

        c3 =
            if (y0 > z0) then
                8
            else
                1

        c4 =
            if (x0 > w0) then
                4
            else
                1

        c5 =
            if (y0 > w0) then
                2
            else
                1

        c6 =
            if (z0 > w0) then
                1
            else
                1

        c =
            c1 + c2 + c3 + c4 + c5 + c6

        i1 =
            (if (iget 0 (aget c simplex) >= 3) then
                1
             else
                0
            )

        j1 =
            (if (iget 1 (aget c simplex) >= 3) then
                1
             else
                0
            )

        k1 =
            (if (iget 2 (aget c simplex) >= 3) then
                1
             else
                0
            )

        l1 =
            (if (iget 3 (aget c simplex) >= 3) then
                1
             else
                0
            )

        i2 =
            (if (iget 0 (aget c simplex) >= 2) then
                1
             else
                0
            )

        j2 =
            (if (iget 1 (aget c simplex) >= 2) then
                1
             else
                0
            )

        k2 =
            (if (iget 2 (aget c simplex) >= 2) then
                1
             else
                0
            )

        l2 =
            (if (iget 3 (aget c simplex) >= 2) then
                1
             else
                0
            )

        i3 =
            (if (iget 0 (aget c simplex) >= 1) then
                1
             else
                0
            )

        j3 =
            (if (iget 1 (aget c simplex) >= 1) then
                1
             else
                0
            )

        k3 =
            (if (iget 2 (aget c simplex) >= 1) then
                1
             else
                0
            )

        l3 =
            (if (iget 3 (aget c simplex) >= 1) then
                1
             else
                0
            )

        x1 =
            x0 - toFloat i1 + g4

        y1 =
            y0 - toFloat j1 + g4

        z1 =
            z0 - toFloat k1 + g4

        w1 =
            w0 - toFloat l1 + g4

        x2 =
            x0 - toFloat i2 + 2 * g4

        y2 =
            y0 - toFloat j2 + 2 * g4

        z2 =
            z0 - toFloat k2 + 2 * g4

        w2 =
            w0 - toFloat l2 + 2 * g4

        x3 =
            x0 - toFloat i3 + 3 * g4

        y3 =
            y0 - toFloat j3 + 3 * g4

        z3 =
            z0 - toFloat k3 + 3 * g4

        w3 =
            w0 - toFloat l3 + 3 * g4

        x4 =
            x0 - 1 + 4 * g4

        y4 =
            y0 - 1 + 4 * g4

        z4 =
            z0 - 1 + 4 * g4

        w4 =
            w0 - 1 + 4 * g4

        ii =
            modBy 256 i

        jj =
            modBy 256 j

        kk =
            modBy 256 k

        ll =
            modBy 256 l

        gi0 =
            modBy 32 (iget (ii + (iget (jj + (iget (kk + (iget ll perm)) perm)) perm)) perm)

        gi1 =
            modBy 32 (iget (ii + i1 + (iget (jj + j1 + (iget (kk + k1 + (iget (ll + l1) perm)) perm)) perm)) perm)

        gi2 =
            modBy 32 (iget (ii + i1 + (iget (jj + j2 + (iget (kk + k2 + (iget (ll + l2) perm)) perm)) perm)) perm)

        gi3 =
            modBy 32 (iget (ii + i1 + (iget (jj + j3 + (iget (kk + k3 + (iget (ll + l3) perm)) perm)) perm)) perm)

        gi4 =
            modBy 32 (iget (ii + i1 + (iget (jj + 1 + (iget (kk + 1 + (iget (ll + 1) perm)) perm)) perm)) perm)

        t0 =
            0.5 - x0 * x0 - y0 * y0 - z0 * z0 - w0 * w0

        t1 =
            0.5 - x1 * x1 - y1 * y1 - z1 * z1 - w1 * w1

        t2 =
            0.5 - x2 * x2 - y2 * y2 - z2 * z2 - w2 * w2

        t3 =
            0.5 - x3 * x3 - y3 * y3 - z3 * z3 - w3 * w3

        t4 =
            0.5 - x4 * x4 - y4 * y4 - z4 * z4 - w4 * w4

        n0 =
            if (t0 < 0) then
                0
            else
                (t0 ^ 4) * (dot (lget gi0 gradients4D) [ x0, y0, z0, w0 ])

        n1 =
            if (t1 < 0) then
                0
            else
                (t1 ^ 4) * (dot (lget gi1 gradients4D) [ x1, y1, z1, w1 ])

        n2 =
            if (t2 < 0) then
                0
            else
                (t2 ^ 4) * (dot (lget gi2 gradients4D) [ x2, y2, z2, w2 ])

        n3 =
            if (t3 < 0) then
                0
            else
                (t3 ^ 4) * (dot (lget gi3 gradients4D) [ x3, y3, z3, w3 ])

        n4 =
            if (t4 < 0) then
                0
            else
                (t4 ^ 4) * (dot (lget gi4 gradients4D) [ x4, y4, z4, w4 ])
    in
        27 * (n0 + n1 + n2 + n3 + n4)


gradients3D : Array (Array Float)
gradients3D =
    (Array.fromList >> Array.map Array.fromList)
        [ [ 1, 1, 0 ]
        , [ -1, 1, 0 ]
        , [ 1, -1, 0 ]
        , [ -1, -1, 0 ]
        , [ 1, 0, 1 ]
        , [ -1, 0, 1 ]
        , [ 1, 0, -1 ]
        , [ -1, 0, -1 ]
        , [ 0, 1, 1 ]
        , [ 0, -1, 1 ]
        , [ 0, 1, -1 ]
        , [ 0, -1, -1 ]
        ]


gradients4D : Array (Array Float)
gradients4D =
    (Array.fromList >> Array.map Array.fromList)
        [ [ 0, 1, 1, 1 ]
        , [ 0, 1, 1, -1 ]
        , [ 0, 1, -1, 1 ]
        , [ 0, 1, -1, -1 ]
        , [ 0, -1, 1, 1 ]
        , [ 0, -1, 1, -1 ]
        , [ 0, -1, -1, 1 ]
        , [ 0, -1, -1, -1 ]
        , [ 1, 0, 1, 1 ]
        , [ 1, 0, 1, -1 ]
        , [ 1, 0, -1, 1 ]
        , [ 1, 0, -1, -1 ]
        , [ -1, 0, 1, 1 ]
        , [ -1, 0, 1, -1 ]
        , [ -1, 0, -1, 1 ]
        , [ -1, 0, -1, -1 ]
        , [ 1, 1, 0, 1 ]
        , [ 1, 1, 0, -1 ]
        , [ 1, -1, 0, 1 ]
        , [ 1, -1, 0, -1 ]
        , [ -1, 1, 0, 1 ]
        , [ -1, 1, 0, -1 ]
        , [ -1, -1, 0, 1 ]
        , [ -1, -1, 0, -1 ]
        , [ 1, 1, 1, 0 ]
        , [ 1, 1, -1, 0 ]
        , [ 1, -1, 1, 0 ]
        , [ 1, -1, -1, 0 ]
        , [ -1, 1, 1, 0 ]
        , [ -1, 1, -1, 0 ]
        , [ -1, -1, 1, 0 ]
        , [ -1, -1, -1, 0 ]
        ]


perm : Array Int
perm =
    Array.fromList
        [ 151, 160, 137, 91, 90, 15, 131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23, 190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33, 88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166, 77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244, 102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196, 135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123, 5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42, 223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9, 129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228, 251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107, 49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254, 138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180, 151, 160, 137, 91, 90, 15, 131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36, 103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23, 190, 6, 148, 247, 120, 234, 75, 0, 26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33, 88, 237, 149, 56, 87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166, 77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55, 46, 245, 40, 244, 102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132, 187, 208, 89, 18, 169, 200, 196, 135, 130, 116, 188, 159, 86, 164, 100, 109, 198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123, 5, 202, 38, 147, 118, 126, 255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42, 223, 183, 170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43, 172, 9, 129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112, 104, 218, 246, 97, 228, 251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162, 241, 81, 51, 145, 235, 249, 14, 239, 107, 49, 192, 214, 31, 181, 199, 106, 157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254, 138, 236, 205, 93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180 ]


simplex : Array (Array Int)
simplex =
    (Array.fromList >> Array.map Array.fromList)
        [ [ 0, 1, 2, 3 ]
        , [ 0, 1, 3, 2 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 2, 3, 1 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 1, 2, 3, 0 ]
        , [ 0, 2, 1, 3 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 3, 1, 2 ]
        , [ 0, 3, 2, 1 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 1, 3, 2, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 1, 2, 0, 3 ]
        , [ 0, 0, 0, 0 ]
        , [ 1, 3, 0, 2 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 2, 3, 0, 1 ]
        , [ 2, 3, 1, 0 ]
        , [ 1, 0, 2, 3 ]
        , [ 1, 0, 3, 2 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 2, 0, 3, 1 ]
        , [ 0, 0, 0, 0 ]
        , [ 2, 1, 3, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 2, 0, 1, 3 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 3, 0, 1, 2 ]
        , [ 3, 0, 2, 1 ]
        , [ 0, 0, 0, 0 ]
        , [ 3, 1, 2, 0 ]
        , [ 2, 1, 0, 3 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 0, 0, 0, 0 ]
        , [ 3, 1, 0, 2 ]
        , [ 0, 0, 0, 0 ]
        , [ 3, 2, 0, 1 ]
        , [ 3, 2, 1, 0 ]
        ]
