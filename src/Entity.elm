module Entity exposing (Entity(..), addGen, attach, detach, empty, get, has)

import Component as Cmp exposing (Component(..))
import Random exposing (Generator)


type Entity
    = Entity (List Component)


addGen : (a -> Component) -> Generator a -> Entity -> Generator Entity
addGen comp gen entity =
    Random.map
        (\v -> attach (comp v) entity)
        gen


attach : Component -> Entity -> Entity
attach comp (Entity components) =
    Entity (comp :: components)


detach : String -> Entity -> Entity
detach cmp (Entity components) =
    components
        |> List.filter (Cmp.is cmp >> not)
        |> Entity


empty : Entity
empty =
    Entity []


get : String -> Entity -> Maybe Component
get cmp (Entity components) =
    components
        |> List.filter (\c -> Cmp.is cmp c)
        |> List.head


has : String -> Entity -> Bool
has cmp (Entity components) =
    components
        |> List.any (\c -> Cmp.is cmp c)
